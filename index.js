/*
	[SECTION] - Parameters and Arguments
	js functions are lines/blocks of code that tell the device/application to perform certain tasks.

	// SAMPLE COMMENTED
	function printInput(){
		let nickname = prompt("Enter your nickname.");
		console.log("Hi, " + nickname);
	}
	printInput();
*/

// some cases, basic functions may not be ideal.
// other cases, functions can also process data directly into it instead.

// 				Parameter
//                  |
//                  V
function printName(name){
	console.log("My name is " + name);
}

//     argument 
//         |
//         V
printName("Ashley");
// the code is more flexible and versatile

let sampleVariable = "Mae";

printName(sampleVariable);

function checkDivisibleBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is ..." + remainder);
	
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibleBy8(64);
checkDivisibleBy8(28);

// functions as arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message printed.");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);
// structure is shown
console.log(argumentFunction);

// function - multiple parameters
// multiple arguments will correspond to the number of "parameters"
function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Ashley", "Chua", "Pancho");
createFullName("Ashley");
createFullName("Ashley", "Chua", "Pancho", "Dumb");

let firstName = "Joe";
let middleName = "Doe";
let lastName = "Mama";

createFullName(firstName, middleName, lastName);
createFullName(firstName, "Chua", "Pancho");

// parameter names refer to the arguments.
// the order of the argument is the same order of the parameter
function printFullName(middleName, firstName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("John", "Dela", "Ruz");

// the return statement
function returnFullName(firstName, middleName, lastName){
	console.log("This message will be printed");
	return firstName + " " + middleName +" " + lastName;
	console.log("This message will not be printed");
	// returns are preferable.
}

let completeName = returnFullName("Henwee", "Charwee", "Ashwee");
console.log(completeName);

// returnFullName("Henwee", "Charwee", "Ashwee");
// this will not work if function is expecting return value, returned values should be stored in a value.

console.log(returnFullName(firstName, middleName, lastName));

// it is possible to create a variable inside the function to contain the result and return that variable instead.
function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("QC", "Philippines");
console.log(myAddress);

// on the other hand, when a function only has console.log() to display its result it will return undefined instead.
function printPlayerInfo(username, level, role){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Role: " + role);
}

let user1 = printPlayerInfo("ColeX", 96, "Assassin");
console.log(user1);
// this returns unidentified because printPlayerInfo returns nothing.